<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// F
	'feuillederoute_description' => 'Ce plugin ajoute un bouton d\'administration sur chaque page du site, publique comme privée, pour afficher/masquer un texte simple, hors tout contexte éditorial. Il est sensé être un moyen de communication entre webmestres, administrateurs et rédacteurs.
	Il est compatible avec le plugin Minibando.',
	'feuillederoute_nom' => 'Feuille de Route',
	'feuillederoute_slogan' => 'Un bouton d\'admin pour partager des informations hors publication.'
);