<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C 
	'cfg_titre_feuillederoute' => 'Configurer la Feuille de Route',
	'cfg_form_feuillederoute_autorisations' => 'Autorisations',
	'cfg_lbl_type_autorisation' => 'Méthode d\'autorisation',
	'cfg_inf_type_autorisation' => 'Choisissez un type d\'autorisations. Pour les choix par Statuts et par Liste d\'auteurs, vous pourrez en choisir plusieurs (ctrl + clic)',
	'cfg_lbl_autorisation_auteurs' => 'autoriser par liste d\'auteurs',
	'cfg_lbl_autorisation_statuts' => 'autoriser par statut d\'auteurs',
	'cfg_lbl_autorisation_webmestre' => 'autoriser uniquement les webmestres',
	'cfg_lbl_liste_auteurs' => 'Auteurs du site',
	'cfg_lbl_statuts_auteurs' => 'Statuts possibles',
	'cfg_lgd_autorisation_lire' => 'Lire la Feuille de route',
	'cfg_lgd_autorisation_modifier' => 'Modifier la Feuille de route',
	'cfg_explication_autorisations' => 'Les profils autorisés à modifier la Feuille de Route seront automatiquement autorisés à la lire.',
	'cfg_form_feuillederoute_titre' => 'Le titre de votre Feuille de Route',
	'cfg_explication_titre' => 'Vous pouvez choisir le titre qui sera utilisé dans le bouton d\'ouverture et dans la feuille elle-même.',

	// F
	'feuillederoute' => 'Feuille de Route',

	// M
	'message_ok' => 'Texte modifié avec succès !',
	'modifier_le_texte' => 'Modifier ce texte',

	// T
	'texte' => 'Texte :',
	'titre' => 'Titre :',

	// S
	'submit' => 'Enregistrer',

);